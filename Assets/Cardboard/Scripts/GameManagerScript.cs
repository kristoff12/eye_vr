﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class GameManagerScript : MonoBehaviour {

    private const float MAX_SESSION_TIME_S = 120;
	private GameConfiguration mConfiguration;
    private float mGameplayTimer = 0.0f;

    private DataService dataservice;
    private GameObject mTimerObject;
    private bool mIsGameRunning = true;

    private int mPoints;

    // Use this for initialization
    void Start () {
		var configurationId = PlayerPrefs.GetInt (Constants.CONFIG_ID_KEY);

		if (configurationId > 0) {
			dataservice = new DataService ("eye_vr.db");
			mConfiguration = dataservice.LoadConfiguration (configurationId);
			Debug.Log ("Loaded configuration: " + mConfiguration.ToString ());
			SetupScene (mConfiguration);
            mGameplayTimer = 0.0f;

            // enable cannon control
            GameObject.Find("Head").SendMessage("SetWiezyczkaControlDisabled", false);
        }

        mTimerObject = GameObject.Find("GameTimer");
        mIsGameRunning = true;

        mPoints = 0;
    }
	
	// Update is called once per frame
	void Update () {

        if (!mIsGameRunning)
            return;

        mGameplayTimer += Time.deltaTime;
        if (mGameplayTimer > MAX_SESSION_TIME_S)
        {
            Debug.Log("Time is up!");

            // disable cannon control
            GameObject.Find("Head").SendMessage("SetWiezyczkaControlDisabled", true);

            // stop the game loop
            mIsGameRunning = false;

            // session ended display scene with results
            DisplayGameSessionResultsScene();

        } else
        {
            if (mTimerObject != null)
            {
                mTimerObject.SendMessage("SetTime", mGameplayTimer);
            }
        }
	}

    private void DisplayGameSessionResultsScene()
    {
        // display game session result
        Debug.Log("Displaying game session results");

        // store session time
        dataservice.UpdateSessionTime(mConfiguration.SessionId, mConfiguration.ConfigurationID, mGameplayTimer);

        // store session points
        dataservice.UpdateSessionPoints(mConfiguration.SessionId, mConfiguration.ConfigurationID, mPoints);

        // store data for the summary screen
        PlayerPrefs.SetInt(Constants.SUMMARY_POINTS_KEY, mPoints);
        PlayerPrefs.SetInt(Constants.SESSION_ID_KEY, mConfiguration.SessionId);

        // load the result scene
        SceneManager.LoadScene("SummaryScene");
    }

    private void SetupScene( GameConfiguration pConfiguration){
		// setup background color
		SetupBackgroundColor(pConfiguration.BgColor);
		SetupMeteorGeneration (pConfiguration);
	}

	private void SetupBackgroundColor(string pBackgroundColor){
		Camera[] cameras = Camera.main.GetComponentsInChildren<Camera> (true);

		foreach (Camera cam in cameras){
			switch (pBackgroundColor){
			case "Red":
				cam.backgroundColor = Color.red;
				break;
			case "Blue":
				cam.backgroundColor = Color.blue;
				break;
			case "Yellow":
				cam.backgroundColor = Color.yellow;
				break;
			case "Green":
				cam.backgroundColor = Color.green;
				break;

			}
		}
	}

	private void SetupMeteorGeneration(GameConfiguration pConfiguration){
		GameObject meteorSpawner = GameObject.Find ("MeteorSpawner");
		meteorSpawner.SendMessage ("SetMeteorSpeed", pConfiguration.Speed);
		meteorSpawner.SendMessage ("SetMeteorSize", pConfiguration.Size);
		meteorSpawner.SendMessage ("SetMeteorColor", pConfiguration.ObjectColor);

	}

    public void AddPoints()
    {
        mPoints++;
    }

    public void SetParameterValue(string[] pParamValArray)
    {
        // process messages from UI elements
        if (pParamValArray != null)
        {
            switch (pParamValArray[0])
            {
                case Constants.PAUSE_MENU_VALUE:
                    // pause was pressed - return to the home screen to change configuration
                    PauseGame();
                    break;

            }
        }
    }

    private void PauseGame()
    {
        // store the session time
        dataservice.UpdateSessionTime(mConfiguration.SessionId, mConfiguration.ConfigurationID, mGameplayTimer);

        // store the fact that the game was paused
		PlayerPrefs.SetInt(Constants.GAME_WAS_PAUSED_KEY, 1);

        // close database access
        dataservice.CloseConnection();

        // transition to configuration screen
        SceneManager.LoadScene("ConfigScene");

    }
}
