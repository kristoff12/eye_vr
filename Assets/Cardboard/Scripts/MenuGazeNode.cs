﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuGazeNode : MonoBehaviour, ICardboardGazeResponder {

	public GameObject[] subNodes;

	protected float timeGazedAt = 0.0f;
	protected static float GAZE_TIME_TO_CLICK = 1.0f;
	protected bool isGazedAt = false;
	protected bool isSelected = true;
	protected bool clickPerformed = false;

	// parent object
	private GameObject parentObject = null;

	public Sprite nominalSprite;
	public Sprite selectedSprite;
	public bool ignoreHide = false;

	void Start() {
		SetGazedAt(false);
		UnselectNode ();
		RegisterParentObjectToSubNodes ();
	}

	void Update(){
		if (isGazedAt) {
			timeGazedAt += Time.deltaTime;
			if (timeGazedAt > GAZE_TIME_TO_CLICK) {
				// trigger click
				if (!clickPerformed) {
					PerformClickAction ();
					clickPerformed = true;
				}
			}
		}
	}

	void RegisterParentObjectToSubNodes(){
		foreach (GameObject go in subNodes) {
			go.SendMessage ("SetParentObject", this.gameObject);
		}
	}

	public void UnselectNode(){

		if (!isSelected) {
			return;
		}

		// set the nominal image
		//Debug.Log ("Unselecting node " + this.gameObject.name);
		if (nominalSprite != null) {
			GetComponent<Image> ().sprite = nominalSprite;
		}

		HideSubnodes ();

		isSelected = false;
	}

	void LateUpdate() {
	}

	public void HideSubnodes(){
		if (ignoreHide) {
			return;
		}
		if (subNodes.Length > 0) {
			foreach (GameObject go in subNodes) {
				go.SendMessage ("UnselectNode");
				//Debug.Log ("Hiding node " + go.name);
				go.GetComponent<RectTransform> ().localScale = new Vector3 (0f, 0f, 0f);
			}
		}
	}

	public void SetGazedAt(bool gazedAt) {

		if (!gazedAt) {
			//Debug.Log ("you gazed " + timeGazedAt);
			timeGazedAt = 0.0f;

			// enable sending click
			clickPerformed = false;
		}
		isGazedAt = gazedAt;
	}

	public void Reset() {
	}
		

	#region ICardboardGazeResponder implementation

	/// Called when the user is looking on a GameObject with this script,
	/// as long as it is set to an appropriate layer (see CardboardGaze).
	public void OnGazeEnter() {
		SetGazedAt(true);
	}

	/// Called when the user stops looking on the GameObject, after OnGazeEnter
	/// was already called.
	public void OnGazeExit() {
		SetGazedAt(false);
	}

	// Called when the Cardboard trigger is used, between OnGazeEnter
	/// and OnGazeExit.
	public void OnGazeTrigger() {
		//PerformClickAction ();
	}

	public void PerformClickAction(){

		if (isSelected) {
			// already clicked - exit
			return;
		}

		Debug.Log ("Performing click on: " + this.gameObject.name);

		// if we have sub nodes display them
		if (subNodes.Length > 0) {
			foreach (GameObject go in subNodes) {
				go.GetComponent<RectTransform> ().localScale = new Vector3 (1f, 1f, 1f);
			}
		}

		// set the sprite to active
		if (selectedSprite != null) {
			GetComponent<Image> ().sprite = selectedSprite;
		}

		// notify parent
		if (parentObject != null) {
			parentObject.SendMessage ("SubNodeClicked", this.gameObject);
		}

		// set the selected flag
		isSelected = true;
			
	}

	public void SubNodeClicked(GameObject clickedNode){

		//Debug.Log ("Subnode of " + this.gameObject.name +" was clicked: " + clickedNode.name);

		// deselect all subnodes apart from the clicked node
		foreach (GameObject go in subNodes) {
			if (go != clickedNode) {
				go.SendMessage ("UnselectNode");
			}
		}
	}

	public void SetParentObject( GameObject pParentObject){
		parentObject = pParentObject;
	}

	#endregion
}
