﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SummaryScreenGameManagerScript : MonoBehaviour {

    private int mSessionId;
    private int mPoints;

    // Use this for initialization
    void Start () {
        mPoints = PlayerPrefs.GetInt (Constants.SUMMARY_POINTS_KEY);
        mSessionId = PlayerPrefs.GetInt(Constants.SESSION_ID_KEY);
        DisplayGameSessionResultsText();
    }
	
	// Update is called once per frame
	void Update () {
	}

    private void DisplayGameSessionResultsText()
    {
        GameObject.Find("SessionInformation").GetComponent<Text>().text = String.Format("Sesja numer: {0} zakończona!", mSessionId);
        GameObject.Find("PointInformation").GetComponent<Text>().text = String.Format("Punkty: {0}", mPoints);

    }


    public void SetParameterValue(string[] pParamValArray)
    {
        // process messages from UI elements
        if (pParamValArray != null)
        {
            switch (pParamValArray[0])
            {
                case Constants.NEW_GAME_MENU_VALUE:
                    // new game was pressed - return to the home screen for new user to start
                    NewGame();
                    break;

            }
        }
    }

    private void NewGame()
    {

        // create a new session when going to configuration screen
		PlayerPrefs.SetInt(Constants.GAME_WAS_PAUSED_KEY, 0);

        // transition to configuration screen
        SceneManager.LoadScene("ConfigScene");

    }
}
