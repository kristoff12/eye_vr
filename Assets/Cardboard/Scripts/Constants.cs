﻿using UnityEngine;
using System.Collections;

public class Constants {

	public static string CONFIG_ID_KEY = "CONFIG_ID_KEY";
	internal static readonly string GAME_WAS_PAUSED_KEY = "GAME_WAS_PAUSED_KEY";
    internal const string PAUSE_MENU_VALUE = "Pause";
    internal const string NEW_GAME_MENU_VALUE = "NewGame";
    internal static readonly string SUMMARY_POINTS_KEY = "SUMMARY_POINTS_KEY";
    internal static readonly string SESSION_ID_KEY = "SESSION_ID_KEY";
}
