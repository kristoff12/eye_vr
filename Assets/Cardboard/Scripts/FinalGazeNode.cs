﻿using UnityEngine;
using System.Collections;

public class FinalGazeNode : MenuGazeNode {

	public string ParameterName;
	public string ParameterValue;

	new public void PerformClickAction(){


		base.PerformClickAction ();
		GameObject.Find ("GameManager").SendMessage ("SetParameterValue",new string[] {ParameterName, ParameterValue});
	}

	void Update(){
		if (isGazedAt) {
			timeGazedAt += Time.deltaTime;
			if (timeGazedAt > GAZE_TIME_TO_CLICK) {
				// trigger click
				if (!clickPerformed) {
					PerformClickAction ();
					clickPerformed = true;
				}
			}
		}
	}
}
