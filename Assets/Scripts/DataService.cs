﻿using SQLite4Unity3d;
using UnityEngine;
#if !UNITY_EDITOR
using System.Collections;
using System.IO;
#endif
using System.Collections.Generic;

public class DataService  {

	private SQLiteConnection _connection;

	public DataService(string DatabaseName){

#if UNITY_EDITOR
            var dbPath = string.Format(@"Assets/StreamingAssets/{0}", DatabaseName);
#else
        // check if file exists in Application.persistentDataPath
        var filepath = string.Format("{0}/{1}", Application.persistentDataPath, DatabaseName);

        if (!File.Exists(filepath))
        {
            Debug.Log("Database not in Persistent path");
            // if it doesn't ->
            // open StreamingAssets directory and load the db ->

#if UNITY_ANDROID 
            var loadDb = new WWW("jar:file://" + Application.dataPath + "!/assets/" + DatabaseName);  // this is the path to your StreamingAssets in android
            while (!loadDb.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
            // then save to Application.persistentDataPath
            File.WriteAllBytes(filepath, loadDb.bytes);
#elif UNITY_IOS
                 var loadDb = Application.dataPath + "/Raw/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
                // then save to Application.persistentDataPath
                File.Copy(loadDb, filepath);
#elif UNITY_WP8
                var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
                // then save to Application.persistentDataPath
                File.Copy(loadDb, filepath);

#elif UNITY_WINRT
		var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);
#else
	var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
	// then save to Application.persistentDataPath
	File.Copy(loadDb, filepath);

#endif

            Debug.Log("Database written");
        }

        var dbPath = filepath;
#endif
            _connection = new SQLiteConnection(dbPath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create);
        Debug.Log("Final PATH: " + dbPath);    

		//check if table exists
		var tableInfo = _connection.GetTableInfo ("GameConfiguration");
		if (tableInfo == null || tableInfo.Count == 0) {
			
			// create the table
			Debug.Log ("Database table not existant - creating");
			_connection.CreateTable<GameConfiguration> ();
		}

	}

//	public void CreateDB(){
//		_connection.DropTable<GameConfiguration> ();
//		_connection.CreateTable<GameConfiguration> ();
//	}
//
//	public IEnumerable<GameConfiguration> GetAllConfigurations(){
//		return _connection.Table<GameConfiguration>();
//	}
//
//	public IEnumerable<Person> GetPersonsNamedRoberto(){
//		return _connection.Table<Person>().Where(x => x.Name == "Roberto");
//	}
//
//	public Person GetJohnny(){
//		return _connection.Table<Person>().Where(x => x.Name == "Johnny").FirstOrDefault();
//	}
//
//	public GameConfiguration CreatePerson(){
//		var p = new Person{
//				Name = "Johnny",
//				Surname = "Mnemonic",
//				Age = 21
//		};
//		_connection.Insert (p);
//		return p;
//	}

	public int StoreGameConfiguration (GameConfiguration pGameConfiguration){
        Debug.Log("Storing game configuration " + pGameConfiguration.ToString());

		_connection.Insert (pGameConfiguration);
		return pGameConfiguration.ConfigurationID;
	}

	public int GetLastStoredGameConfigurationID(){
		if (_connection.Table<GameConfiguration> ().Count () > 0) {
			return _connection.Table<GameConfiguration> ().ElementAt (
					_connection.Table<GameConfiguration> ().Count () - 1).ConfigurationID;
		} else {
			return 0;
		}
	}

   public int GetSessionNumberForConfiguration(int pConfigurationID)
    {
        var config =_connection.Table<GameConfiguration>().Where(x => x.ConfigurationID == pConfigurationID).FirstOrDefault();
        if (config != null) {
            return config.SessionId;
        } else
        {
            return 0;
        }
        
    }

    public void CloseConnection(){
		_connection.Close ();
	}

	public GameConfiguration LoadConfiguration(int pConfigurationID){
		return _connection.Table<GameConfiguration>().Where(x => x.ConfigurationID == pConfigurationID).FirstOrDefault();
	}

	public int GetLastSessionID(){
		if (_connection.Table<GameConfiguration> ().Count () > 0) {
			return _connection.Table<GameConfiguration> ().ElementAt (
		    _connection.Table<GameConfiguration> ().Count () - 1).SessionId;
		} else {
			return 0;
		}
	}

	// store the session time
	public void UpdateSessionTime (int pSessionID, int pConfigurationID, float pSessionTimeInSeconds){

        // Updating the session time
        Debug.Log(string.Format("Updating the session {0} with configuration {1} gameplay time to {2} ", pSessionID, pConfigurationID, pSessionTimeInSeconds));
		GameConfiguration gc = _connection.Table<GameConfiguration> ().Where (x => x.SessionId == pSessionID && x.ConfigurationID == pConfigurationID).FirstOrDefault ();
		gc.SessionTimeInSeconds = pSessionTimeInSeconds;
        _connection.Update(gc);
	}

    // store the session points
    public void UpdateSessionPoints(int pSessionID, int pConfigurationID, int pSessionPoints)
    {

        // Updating the session time
        Debug.Log(string.Format("Updating the session {0} with configuration {1} points to {2} ", pSessionID, pConfigurationID, pSessionPoints));
        GameConfiguration gc = _connection.Table<GameConfiguration>().Where(x => x.SessionId == pSessionID && x.ConfigurationID == pConfigurationID).FirstOrDefault();
        gc.Points = pSessionPoints;
        _connection.Update(gc);
    } 
}
