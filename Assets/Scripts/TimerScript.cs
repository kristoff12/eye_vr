﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

[RequireComponent(typeof(Text))]
public class TimerScript : MonoBehaviour {
	private Text textField;
	private float mSeconds = 0;

	void Awake() {
		textField = GetComponent<Text>();

	}

	void LateUpdate() {

        TimeSpan t = TimeSpan.FromSeconds(mSeconds);

        textField.text = string.Format("{0}:{1:D2}",
                        t.Minutes,
                        t.Seconds);
	}

	public void SetTime(float pSeconds){
        mSeconds = pSeconds;
	}
}
