﻿using SQLite4Unity3d;

public class GameConfiguration  {

	[PrimaryKey, AutoIncrement]
	public int ConfigurationID { get; set; }
	public int SessionId { get; set; }
	public string BgColor { get; set; }
	public string ObjectColor { get; set; }
	public string CrosshairColor { get; set; }
	public int Size { get; set; }
	public int Speed { get; set; }
	public int Points { get; set; }
	public float SessionTimeInSeconds { get; set;}

	public override string ToString ()
	{
		return string.Format ("[GameConfiguration: ConfigurationID={0}, SessionId={1}, BgColor={2},  ObjectColor={3}, CrosshairColor={4}, Size={5}, Speed={6}, Points={7}, SessionTimeInSeconds={8}]",
			ConfigurationID,
			SessionId,
			BgColor, 
			ObjectColor, 
			CrosshairColor,
			Size,
			Speed,
			Points,
			SessionTimeInSeconds
		);
	}

	public GameConfiguration(){
		ConfigurationID = 1;
		BgColor = "Blue";
		ObjectColor = "Green";
		CrosshairColor = "Red";
		Size = 1;
		Speed = 1;
		Points = 0;
		SessionTimeInSeconds = 0;
	}
}
