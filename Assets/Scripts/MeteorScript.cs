﻿using UnityEngine;
using System.Collections;

public class MeteorScript : MonoBehaviour, ICardboardGazeResponder {

	GameObject meteorSpawner;
	GameObject mPointsObject;
    GameObject mGameManager;

	protected float timeGazedAt = 0.0f;
	protected static float GAZE_TIME_TO_CLICK = 1.0f;
	protected bool isGazedAt = false;

	// Use this for initialization
	void Start () {

		// set random rotation
		meteorSpawner = GameObject.Find ("MeteorSpawner");
		GetComponent<Rigidbody> ().angularVelocity = new Vector3 (
			Random.Range (4, 10), 
			Random.Range (4, 10), 
			Random.Range (4, 10));

		mPointsObject = GameObject.Find ("Points");

        mGameManager = GameObject.Find("GameManager");

    }
	
	// Update is called once per frame
	void Update () {
		if (Vector3.Distance (transform.position, meteorSpawner.transform.position) > 50.0f) {
			// meteor too far destroy it
			DestroyMeteorNoPoints();
		}
			
		if (isGazedAt) {
			timeGazedAt += Time.deltaTime;
			if (timeGazedAt > GAZE_TIME_TO_CLICK) {
				// destroy meteor
				DestroyMeteor ();
			}
		}
	}

	private void DestroyMeteor(){
		
		// add points
		mPointsObject.SendMessage("AddPoints");
        mGameManager.SendMessage("AddPoints");

        DestroyMeteorNoPoints ();

	}

	private void DestroyMeteorNoPoints(){
		meteorSpawner.SendMessage ("MeteorDestroyed");
		Debug.Log ("Meteor destroyed");
		Destroy (this.gameObject);
	}

	#region ICardboardGazeResponder implementation

	/// Called when the user is looking on a GameObject with this script,
	/// as long as it is set to an appropriate layer (see CardboardGaze).
	public void OnGazeEnter() {
		SetGazedAt(true);
	}

	/// Called when the user stops looking on the GameObject, after OnGazeEnter
	/// was already called.
	public void OnGazeExit() {
		SetGazedAt(false);
	}

	// Called when the Cardboard trigger is used, between OnGazeEnter
	/// and OnGazeExit.
	public void OnGazeTrigger() {
	}

	#endregion

	public void SetGazedAt(bool gazedAt) {

		if (!gazedAt) {
			Debug.Log ("you targeted the meteor for " + timeGazedAt);
			timeGazedAt = 0.0f;
		}
		isGazedAt = gazedAt;
	}
}
