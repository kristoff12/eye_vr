﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlaneController : MonoBehaviour {

	private float TURN_RADIUS_FACTOR = 1.0f;

	private Vector3 mCurrentWaypoint = Vector3.zero;
	public int mVelocity = 10;

	private BezierPath mPath = new BezierPath();
	private float mArcTraversalTime = 0f;
	private float mCurrentArcTraversalTime = 0f;

	// dummies for debug
	private GameObject mTurnDummy = null;
	private GameObject mWaypointDummy = null;

	// Use this for initialization
	void Start () {
		GetComponent<Rigidbody> ().velocity = transform.forward * mVelocity;
		CreateNextWaypoint ();
	}
	
	// Update is called once per frame
	void Update () {
		mCurrentArcTraversalTime += Time.deltaTime;
		float arcTraversalProgress = mCurrentArcTraversalTime / mArcTraversalTime;

		if (arcTraversalProgress <= 1f){

			Vector3 target = mPath.CalculateBezierPoint (0, arcTraversalProgress);

			// calculate rotation vector
			float step = mVelocity * Time.deltaTime * 100;
			Vector3 rotation = Vector3.RotateTowards (transform.forward, target, step, 4);
			transform.rotation = Quaternion.Euler (rotation);

			// and set position
			transform.position = target;
		} else {
			CreateNextWaypoint();
		}
	}

	// randomize the next waypoint
	// for now this is pure random
	void CreateNextWaypoint(){

		float x = Random.Range (-20f, 20f);
		float y = Random.Range (-20f, 20f);
		float z = Random.Range (-20f, 20f);

		mCurrentWaypoint = new Vector3 (x, y, z);

		//we create the data needed for bezier calculation
		List<Vector3> controlPoints = new List<Vector3>();

		// current position
		controlPoints.Add (transform.position);

		// turn approximation
		Vector3 turnApproximationPoint = transform.position + transform.forward * TURN_RADIUS_FACTOR * mVelocity;
		controlPoints.Add (turnApproximationPoint);

		// destination waypoint
		controlPoints.Add(mCurrentWaypoint);

		// dummy point we need 4
		controlPoints.Add(mCurrentWaypoint);

		// set the distance
		float distance = Vector3.Distance(transform.position, turnApproximationPoint) + 
			Vector3.Distance(mCurrentWaypoint, turnApproximationPoint);

		mArcTraversalTime = distance / mVelocity;

		mCurrentArcTraversalTime = 0f;

		// create helpers for tracing of motion
		CreateDebugHelpers (turnApproximationPoint, mCurrentWaypoint);

		mPath.SetControlPoints (controlPoints);
	}

	private void CreateDebugHelpers(Vector3 pTurnApproxPoint, Vector3 pCurrentWaypoint){

		if (mTurnDummy == null) {
			mTurnDummy = GameObject.CreatePrimitive (PrimitiveType.Sphere);
			mTurnDummy.transform.position = pTurnApproxPoint;
		} else {
			mTurnDummy.transform.position = pTurnApproxPoint;
		}

		if (mWaypointDummy == null) {
			mWaypointDummy = GameObject.CreatePrimitive (PrimitiveType.Capsule);
			mWaypointDummy.transform.position = pCurrentWaypoint;
		} else {
			mWaypointDummy.transform.position = pCurrentWaypoint;
		}
	}
}
