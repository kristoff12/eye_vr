﻿using UnityEngine;
using System.Collections;

public class MeteorSpawnerScript : MonoBehaviour {

	public GameObject[] smallMeteors;
	public GameObject[] mediumMeteors;
	public GameObject[] bigMeteors;

	private float mMeteorSpeed;
	private int mMeteorSize;
	public int MAX_METEORS = 10;
	private int mSpawnedMeteors = 0;
	public float maxSpawningRadius = 10.0f;
	public float minSpawningRadius = 3.0f;
	public float modelScaleFactor = 0.1f;
	public float speedFactor = 3;
	private string mMeteorColor;


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
		// perform spawning
		SpawnMeteors();	
	}

	public void SetMeteorSpeed (float pSpeed){
		mMeteorSpeed = pSpeed;
	}

	public void SetMeteorSize (int pSize){
		mMeteorSize = pSize;
	}

	public void SetMeteorColor (string pColor){
		mMeteorColor = pColor;
	}

	private void SpawnMeteors(){
		if (mSpawnedMeteors < MAX_METEORS) {
			for (int i = mSpawnedMeteors; i < MAX_METEORS; ++i) {
				
				// randomize meteor
				GameObject meteor = null;
				switch (mMeteorSize) {
				case 1:
					meteor = smallMeteors [Random.Range (0, smallMeteors.Length)];
					break;
				
				case 2:
					meteor = mediumMeteors [Random.Range (0, mediumMeteors.Length)];
					break;
				
				case 3:
					meteor = bigMeteors [Random.Range (0, bigMeteors.Length)];
					break;

				}

				if (meteor == null) {
					Debug.Log ("Wrong meteor size. Requested size: " + mMeteorSize);
					return;
				}

				// select location of spawning
				Vector3 location = new Vector3 (
					                   transform.position.x +
									   Mathf.Lerp (- maxSpawningRadius, maxSpawningRadius, Random.value),
					                   transform.position.y +
					                   Mathf.Lerp (-maxSpawningRadius, maxSpawningRadius, Random.value),
									   transform.up.magnitude +
					                   Mathf.Lerp (-maxSpawningRadius, maxSpawningRadius, Random.value));

				if (Vector3.Distance (location, transform.position) < minSpawningRadius) {
					// artifficially move
					location.x = transform.position.x + minSpawningRadius;
				}

				meteor = (GameObject)GameObject.Instantiate (meteor,location,Quaternion.identity);

				Rigidbody meteorRigidBody = meteor.GetComponent<Rigidbody> ();
				meteor.transform.rotation = Random.rotation;
				meteorRigidBody.velocity = meteor.transform.forward * mMeteorSpeed * speedFactor;
				meteor.transform.localScale = meteor.transform.localScale * modelScaleFactor;

				Color meteorColor = GetMeteorColor (mMeteorColor);
				meteor.GetComponentInChildren<MeshRenderer> ().material.color = meteorColor;

				mSpawnedMeteors++;

			}
		}
	}
	private Color GetMeteorColor (string pColorString){
		switch (pColorString){
		case "Red":
			return Color.red;

		case "Blue":
			return Color.blue;

		case "Yellow":
			return Color.yellow;

		case "Green":
			return Color.green;
		
		default:
			return Color.blue;
		}
	}

	public void MeteorDestroyed(){
		mSpawnedMeteors--;
	}
}
