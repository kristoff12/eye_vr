﻿using UnityEngine;
using System.Collections;

public class HeadTracker : MonoBehaviour {

	private GameObject wiezyczka;
    private bool mIsControlDisabled = false;

	// Use this for initialization
	void Start () {
		wiezyczka = GameObject.Find ("wiezyczka");
	}

    public void SetWiezyczkaControlDisabled (bool pIsDisabled)
    {
        Debug.Log("Setting tower head control disabled to: " + pIsDisabled);
        mIsControlDisabled = pIsDisabled;
    }
	
	// Update is called once per frame
	void Update () {
		if (wiezyczka == null || mIsControlDisabled) {
			return;
		}
		wiezyczka.transform.rotation = this.transform.rotation;
	}
}
