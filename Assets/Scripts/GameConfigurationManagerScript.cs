﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameConfigurationManagerScript : MonoBehaviour {

    private GameConfiguration gameConfiguration;
    private DataService dataservice;
	private bool mGameWasPaused = false;

    // Use this for initialization
    void Start() {

        gameConfiguration = new GameConfiguration();
        dataservice = new DataService("eye_vr.db");

        int sessionID = dataservice.GetLastSessionID();
		mGameWasPaused = PlayerPrefs.GetInt (Constants.GAME_WAS_PAUSED_KEY, 0) == 1;

		if (sessionID == 0 || !mGameWasPaused)
        {
            // if we should create a new session on entering the config view
            sessionID++;
            Debug.Log("Created new session with ID: " + sessionID);
		} else if (mGameWasPaused)
        {
            // the session number is preserved when pausing, we need to toggle the flag
			PlayerPrefs.SetInt(Constants.GAME_WAS_PAUSED_KEY, 0);

			// load the last game configuration as we are altering the last config
			gameConfiguration = dataservice.LoadConfiguration(dataservice.GetLastStoredGameConfigurationID ());
        }

		gameConfiguration.SessionId = sessionID;
    }
	
	// Update is called once per frame
	void Update () {
	
	}		

	public void SetParameterValue(string[] pParameters){

		string pParameterName = pParameters [0];
		string pValue = pParameters [1];

		Debug.Log ("Setting game config: "+ pParameterName + " setting to " + pValue);

		if (pParameterName.Equals ("BgColor")) {
			gameConfiguration.BgColor = pValue;
		} else if (pParameterName.Equals ("ObjColor")) {
			gameConfiguration.ObjectColor = pValue;
		} else if (pParameterName.Equals ("CrossHairColor")) {
			gameConfiguration.CrosshairColor = pValue;
		} else if (pParameterName.Equals ("Size")) {
			gameConfiguration.Size = int.Parse (pValue);
		} else if (pParameterName.Equals ("Speed")) {
			gameConfiguration.Speed = int.Parse (pValue);
		} else if (pParameterName.Equals ("Start")) {
            //int configId = GetLatestConfigurationIDFromDBOrStoreDefault (gameConfiguration);
            LaunchGame();
			return;
		}

		// store change in database
		StoreConfigurationInDB (gameConfiguration);
	}

	private int StoreConfigurationInDB(GameConfiguration pGameConfiguration){
		int storedId = dataservice.StoreGameConfiguration (pGameConfiguration);
		Debug.Log ("Game configuration stored with id: " + storedId);

		return storedId;
	}

	private int GetLatestConfigurationIDFromDBOrStoreDefault(GameConfiguration pGameConfiguration){

		Debug.Log ("Fetching last saved configuration");
		int storedId = dataservice.GetLastStoredGameConfigurationID();

        // we have to check if there is no configuration or there was no stored configuration for the new session
        if (storedId == 0 || dataservice.GetSessionNumberForConfiguration(storedId) != pGameConfiguration.SessionId) {
            
			Debug.Log ("Storing configuration: " + pGameConfiguration.ToString ());
			storedId = dataservice.StoreGameConfiguration (pGameConfiguration);
			Debug.Log ("Game configuration stored with id: " + storedId);
		}

		return storedId;
	}

	private void LaunchGame(){
        int configurationID = GetLatestConfigurationIDFromDBOrStoreDefault(gameConfiguration);

        PlayerPrefs.SetInt (Constants.CONFIG_ID_KEY, configurationID);
		dataservice.CloseConnection ();
		SceneManager.LoadScene ("GameScene");
	}
}
