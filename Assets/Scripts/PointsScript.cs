﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class PointsScript : MonoBehaviour {
	private Text textField;
	private int mPoints = 0;

	void Awake() {
		textField = GetComponent<Text>();

	}

	void LateUpdate() {

		textField.text = mPoints.ToString ();
	}

	public void AddPoints(){
		mPoints++;
	}
}
